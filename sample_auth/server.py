# -*- coding: utf-8 -*-
import bottle


# BASIC認証のユーザ名とパスワード
USERNAME = "user"
PASSWORD = "passwd"


def check(username, password):
    u"""
    BASIC認証のユーザ名とパスワードをチェック
    @bottle.auth_basic(check)で適用
    """
    return username == USERNAME and password == PASSWORD


@bottle.route("/auth")
@bottle.auth_basic(check)
def hello():
    return "ok"


if __name__ == '__main__':
    bottle.run(host='sample_auth', port=80, debug=True)
