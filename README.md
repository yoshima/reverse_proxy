# nginxでアクセスのあれこれ
## 認証を外に問い合わせ
`ngx_http_auth_request_module`というモジュールを使えばできそう
`auth_request`が認証サーバ．まず認証サーバにアクセスが行き，200が返ってきたらアクセス許可それ以外なら拒否と判断してくれる.

今回は簡単のためBasic認証だが他の認証方式でも可能（参考: http://lamanotrama.hateblo.jp/entry/2016/01/18/142116 )

## 全てのアプリのプロキシ先を明示しなくてもリバプロできる
`rewrite`で頑張れば良さそう．
```
rewrite ^/sample_converter/(.*)$ $1 break;
proxy_pass http://sample_converter$1/ ;
```
これで`http://アドレス/sample_conveter/コンバータID` を`http://sample_conveterコンバータID` へ転送となる．
アクセスのたびにDNSへ問い合わせが必要なので`resolver`を忘れずに書く．
rewriteには正規表現が利用可能．（参考: http://www.skyarch.net/blog/?p=7088)
# Poc
`docker-compose up -d`で

* proxy (nginx)
* sample auth (basic認証を行うサーバ)
* sample_conveter1
* sample_conveter2
* sample_vote

が立ち上がる．

`localhost/vote` or `localhost/sample_conveter/1` or `localhost/sample_conveter/2`
でbasic認証要求．

* ユーザ名: `user`
* パスワード: `passwd`

で認証.

`localhost/vote`でサンプルアプリにリダイレクト．

`localhost/sample_conveter/1`でsample_conveter1に，`localhost/sample_conveter/2`でsample_conveter2にそれぞれリダイレクト．



